include:
  - .gitlab/ci/dind.yml

stages:
  - build-cli
  - build-fpm
  - build-apache
  - release

variables:
  DOCKER_BUILDKIT: 1

.env-init:
  - &env-init |-
    PHP_VERSION=${CI_JOB_NAME#*:}
    PHP_VERSION=${PHP_VERSION%-*}
    PKG_VERSION=${PHP_VERSION/./}

    SAPI=${CI_JOB_NAME#*:$PHP_VERSION-}
    SAPI=${SAPI%/*}

    DOCKERFILE=$SAPI/Dockerfile
    CONTAINER_BUILD_IMAGE=${CONTAINER_BUILD_IMAGES}_$PHP_VERSION-$SAPI
    echo $CONTAINER_BUILD_IMAGE

    # ARGV will be used as docker build args
    set -- "$@" --build-arg=CI_REGISTRY=$CI_REGISTRY
    set -- "$@" --build-arg=PHP_VERSION=$PHP_VERSION
    set -- "$@" --build-arg=PKG_VERSION=$PKG_VERSION

.docker-build:
  - &docker-build |-
    docker_build_image "$@" $SAPI -f $DOCKERFILE

.push-images:
  - &push_images |-
    PHP_VERSION=${CI_JOB_NAME#*:}

    push_sapi_image cli
    push_sapi_image fpm
    push_sapi_image apache

# template for build
.build: &build
  extends: .dind
  stage: build
  script:
    - *env-init
    - set -- "$@" --build-arg=CLI_IMAGE=${CONTAINER_BUILD_IMAGES}_$PHP_VERSION-cli
    - *docker-build

.build-cli: &build-cli
  <<: *build
  stage: build-cli
  script:
    - *env-init
    - *docker-build

.build-fpm: &build-fpm
  <<: *build
  stage: build-fpm

.build-apache: &build-apache
  <<: *build
  stage: build-apache

php:5.3-cli: *build-cli
php:5.6-cli: *build-cli
php:7.1-cli: *build-cli
php:7.2-cli: *build-cli
php:7.3-cli: *build-cli
php:7.4-cli: *build-cli
php:8.0-cli: *build-cli
php:8.1-cli: *build-cli
php:8.2-cli: *build-cli

php:5.3-fpm: *build-fpm
php:5.6-fpm: *build-fpm
php:7.1-fpm: *build-fpm
php:7.2-fpm: *build-fpm
php:7.3-fpm: *build-fpm
php:7.4-fpm: *build-fpm
php:8.0-fpm: *build-fpm
php:8.1-fpm: *build-fpm
php:8.2-fpm: *build-fpm

php:5.3-apache: *build-apache
php:5.6-apache: *build-apache
php:7.1-apache: *build-apache
php:7.2-apache: *build-apache
php:7.3-apache: *build-apache
php:7.4-apache: *build-apache
php:8.0-apache: *build-apache
php:8.1-apache: *build-apache
php:8.2-apache: *build-apache

.release: &release
  extends: .dind
  stage: release

# release branch builds and pushes to gitlab registry
.release-branch: &release_branch
  <<: *release
  except:
    - master
    - tags
  script:
    - |
      push_sapi_image() {
        local SAPI=$1
        local source=${CONTAINER_BUILD_IMAGES}_$PHP_VERSION-$SAPI
        local target=${CONTAINER_BRANCH_IMAGES}_$PHP_VERSION-$SAPI

        docker_push_image "$source" "$target"
      }
    - *push_images

# master branch builds and pushes to staging registry
.release-master: &release_master
  <<: *release
  only:
    - master
  script:
    - |
      push_sapi_image() {
        local SAPI=$1
        local source=${CONTAINER_BUILD_IMAGES}_$PHP_VERSION-$SAPI
        local target=$CI_REGISTRY_IMAGE:$PHP_VERSION-$SAPI

        docker_push_image "$source" "$target"
      }
    - *push_images

release branch:5.3: *release_branch
release branch:5.6: *release_branch
release branch:7.1: *release_branch
release branch:7.2: *release_branch
release branch:7.3: *release_branch
release branch:7.4: *release_branch
release branch:8.0: *release_branch
release branch:8.1: *release_branch
release branch:8.2: *release_branch

release master:5.3: *release_master
release master:5.6: *release_master
release master:7.1: *release_master
release master:7.2: *release_master
release master:7.3: *release_master
release master:7.4: *release_master
release master:8.0: *release_master
release master:8.1: *release_master
release master:8.2: *release_master

# vim:ts=2:sw=2:et
