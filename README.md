# PHP Docker Images

Attempts to mimic [php](https://hub.docker.com/_/php/) image.

The principle of these images is to build fat base image that has all
extensions present that we are using across projects, but the extensions are
not enabled by default. The extensions need to be enabled per project in their
build process.

This results shorter CI pipelines and smaller deploy image layers for
production. Also enable only what is needed principle should make
vulnerabilities in modules not reachable. CI job only executes command to
enable module, and the docker image layer is modification of that `.ini` file.

```Dockerfile
# enable extensions required for application
RUN docker-php-ext-enable opcache filter pcre spl openssl phar
```

For Apache, similar procedure exists:
```Dockerfile
# enable extensions required for application
RUN a2enmod env xsendfile
```

List of Apache modules that can be enabled.
- `alias`
- `auth`
- `authz_host`
- `autoindex`
- `deflate`
- `dir`
- `env`
- `expires`
- `filter`
- `headers`
- `ldap`, `authnz_ldap`
- `log_config`
- `proxy`
- `remoteip`
- `rewrite`
- `status`
- `xsendfile`

## Supported docker images

- `php:5.3-cli`
- `php:5.3-fpm`
- `php:5.3-apache`
- `php:5.6-cli`
- `php:5.6-fpm`
- `php:5.6-apache`
- `php:7.1-cli`
- `php:7.1-fpm`
- `php:7.1-apache`
- `php:7.2-cli`
- `php:7.2-fpm`
- `php:7.2-apache`
- `php:7.3-cli`
- `php:7.3-fpm`
- `php:7.3-apache`
- `php:7.4-cli`
- `php:7.4-fpm`
- `php:7.4-apache`
- `php:8.0-cli`
- `php:8.0-fpm`
- `php:8.0-apache`
