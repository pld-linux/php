# syntax = docker/dockerfile:experimental
#
# Requires Docker v18.06 or later and BuildKit mode to use cache mount
# Docker v18.06 also requires the daemon to be running in experimental mode.
#
# $ DOCKER_BUILDKIT=1 docker build cli
#
# See https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/experimental.md

ARG PHP_VERSION=7.1
ARG CI_REGISTRY
ARG CLI_IMAGE=$CI_REGISTRY_IMAGE:$PHP_VERSION-cli

FROM ${CLI_IMAGE}

EXPOSE 80
CMD ["apache2-foreground"]

ARG PHP_VERSION=7.1

RUN --mount=type=cache,id=poldek,target=/var/cache/poldek \
	set -x \
	&& set -- --caplookup --up -O 'keep downloads=yes' \
	&& PKG_VERSION=${PHP_VERSION/./} \
	&& poldek -u "$@" \
		apache-index \
		apache-mod_alias \
		apache-mod_auth \
		apache-mod_authnz_ldap \
		apache-mod_authz_host \
		apache-mod_autoindex \
		apache-mod_deflate \
		apache-mod_dir \
		apache-mod_env \
		apache-mod_expires \
		apache-mod_filter \
		apache-mod_headers \
		apache-mod_log_config \
		apache-mod_php$PKG_VERSION \
		apache-mod_proxy \
		apache-mod_remoteip \
		apache-mod_rewrite \
		apache-mod_status \
		apache-mod_xsendfile \
	# set loglevel to info to be able to see ldap auth errors
	&& sed -i -e 's/^LogLevel.*/LogLevel info/' /etc/httpd/apache.conf \
	# disable some modules, enable with a2enmod later
	&& modules="ldap authnz_ldap rewrite env expires status xsendfile filter deflate log_config autoindex headers proxy" \
	&& install -d /etc/httpd/conf.avail \
	&& for module in $modules; do \
	  mv -v /etc/httpd/conf.d/??_*mod_$module.conf /etc/httpd/conf.avail; \
	done \
	# log to stderr, stdout. one can use volume to setup file based logging
	&& ln -snf /dev/stderr /var/log/httpd/error_log \
	&& ln -snf /dev/stdout /var/log/httpd/access_log \
	&& /usr/lib/rpm/rpmdb-shrink \
	&& rm -vf /var/cache/hrmib/* \
	&& poldek --clean-whole \
	&& exit 0

COPY trusted-proxies.lst /etc/httpd/conf.d
COPY mod_remoteip.conf /etc/httpd/conf.d
COPY httpd.sh /usr/sbin/apache2-foreground
COPY a2enmod.sh /usr/sbin/a2enmod
