#!/bin/sh
set -eu

# emulate behavior of a2enmod
# http://manpages.ubuntu.com/manpages/xenial/man8/a2enmod.8.html
# SYNOPSIS
#       a2enmod [ [-q|--quiet] module]
#       a2dismod [ [-q|--quiet] module]

die() {
    echo >&2 "ERROR: $*"
    exit 1
}

main() {
    local confdir="${APACHE_CONFDIR:-/etc/httpd}"
    local conf_avail=$confdir/conf.avail
    local conf_d=$confdir/conf.d
    local module filename

    for module in "$@"; do
        conf=$(ls $conf_avail/??_mod_$module.conf 2>/dev/null) || die "Can't find module $module"
        test $(echo "$conf" | wc -l) = "1" || die "Matched more than one file: $conf"
        filename=${conf##*/}
        test -L "$conf_d/$filename" && { echo "Aleady enabled: $module ($conf_d/$filename)"; continue; }
        ln -s "$conf" "$conf_d/$filename"
        echo "Enabled: $module"
    done
}

main "$@"
