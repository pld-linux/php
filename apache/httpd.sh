#!/bin/sh

# Apache gets grumpy about PID files pre-existing
rm -vf /var/run/httpd.pid

exec /usr/sbin/httpd -DFOREGROUND "$@"
