# syntax = docker/dockerfile:experimental
#
# Requires Docker v18.06 or later and BuildKit mode to use cache mount
# Docker v18.06 also requires the daemon to be running in experimental mode.
#
# $ DOCKER_BUILDKIT=1 docker build cli
#
# See https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/experimental.md

ARG CI_REGISTRY
ARG BASE_IMAGE=registry.gitlab.com/pld-linux/pld

FROM $BASE_IMAGE AS runtime

FROM runtime AS util-linux
RUN --mount=type=cache,id=poldek,target=/var/cache/poldek \
	poldek -u util-linux

FROM runtime
CMD ["php", "-a"]

# default extensions always present
ARG CORE_EXTENSIONS="pcre spl opcache"

ARG PHP_VERSION=7.1
ENV PHP_VERSION $PHP_VERSION
ARG PKG_VERSION=71
ARG PHP_INI_DIR=/etc/php$PKG_VERSION
ENV PHP_INI_DIR $PHP_INI_DIR

# Copy first, these are used in RUN script
COPY --from=util-linux /usr/bin/getopt /usr/bin
COPY docker-php-ext-install /usr/bin
COPY docker-php-ext-enable /usr/bin

RUN --mount=type=cache,id=poldek,target=/var/cache/poldek \
	set -x \
	&& set -- --caplookup --up -O 'keep downloads=yes' \
	&& export PKG_CACHE=1 \
	# remove current php flavor from exclude list
	# base dockerfile excludes all php versions.
	# each job will remove exclusion for version it attempts to build
	&& PKG_VERSION=${PHP_INI_DIR#/etc/php} \
	&& IGNORE_PACKAGES="systemd-init *php4* *php52* *php53* *php54* *php55* *php56* *php70* *php71* *php72* *php73* *php74* *php80* *php81* *php82* hhvm*" \
	&& IGNORE_PACKAGES=$(echo "$IGNORE_PACKAGES" | sed -e "s/ \*php$PKG_VERSION\*//") \
	&& poldek-config ignore "$IGNORE_PACKAGES" \
	&& poldek -u "$@" php-dirs \
	&& poldek -u "$@" \
		/usr/bin/php \
		ImageMagick-coder-jpeg \
		ImageMagick-coder-png \
		ImageMagick-coder-svg \
		ca-certificates \
		dumb-init \
	&& EXCLUDE_EXT_5_3="ds mongodb" \
	&& EXCLUDE_EXT_5_6="ds xhprof" \
	&& EXCLUDE_EXT_7_1="crack uuid" \
	&& EXCLUDE_EXT_7_2="$EXCLUDE_EXT_7_1 mcrypt" \
	&& EXCLUDE_EXT_7_3="$EXCLUDE_EXT_7_2" \
	&& EXCLUDE_EXT_7_4="$EXCLUDE_EXT_7_3 hash pcre spl" \
	&& EXCLUDE_EXT_8_0="$EXCLUDE_EXT_7_4 json imagick redis solr ssh2" \
	&& EXCLUDE_EXT_8_1="$EXCLUDE_EXT_8_0 ds memcache mongodb mysql translit xdebug xhprof xmlrpc" \
	&& EXCLUDE_EXT_8_2="$EXCLUDE_EXT_8_1" \
	&& EXTENSIONS="\
		bcmath \
		calendar \
		crack \
		ctype \
		curl \
		dba \
		dom \
		ds \
		exif \
		fileinfo \
		filter \
		ftp \
		gd \
		gettext \
		hash \
		iconv \
		imagick \
		imap \
		intl \
		json \
		ldap \
		mbstring \
		mcrypt \
		memcache \
		mongodb \
		mysql \
		mysqli \
		opcache \
		openssl \
		pcntl \
		pcre \
		pdo \
		pdo_mysql \
		pdo_sqlite \
		phar \
		posix \
		redis \
		session \
		simplexml \
		soap \
		sockets \
		solr \
		spl \
		ssh2 \
		tidy \
		tokenizer \
		translit \
		uuid \
		xdebug \
		xhprof \
		xml \
		xmlreader \
		xmlrpc \
		xmlwriter \
		xsl \
		zip \
		zlib \
	" \
	&& eval $(echo EXCLUDE_EXT=\$EXCLUDE_EXT_${PHP_VERSION/./_}) \
	&& for ext in $EXCLUDE_EXT; do \
	  echo "Exclude ext: $ext"; \
	  EXTENSIONS=$(printf " %s " $EXTENSIONS | sed -e "s/ $ext //"); \
	  CORE_EXTENSIONS=$(printf " %s " $CORE_EXTENSIONS | sed -e "s/ $ext //"); \
	done \
	&& docker-php-ext-install $EXTENSIONS \
	# disable all installed extensions by default
	&& install -d $PHP_INI_DIR/conf.avail \
	&& mv -v $PHP_INI_DIR/conf.d/*.ini $PHP_INI_DIR/conf.avail \
	&& { test ! -f $PHP_INI_DIR/conf.avail/timezone.ini || mv -v $PHP_INI_DIR/conf.avail/timezone.ini $PHP_INI_DIR/conf.d; } \
	&& { test ! -f $PHP_INI_DIR/conf.avail/opcache.ini || mv -v $PHP_INI_DIR/conf.avail/opcache.ini $PHP_INI_DIR/conf.d; } \
	&& docker-php-ext-enable $CORE_EXTENSIONS \
	&& /usr/lib/rpm/rpmdb-shrink \
	&& rm -vf /var/cache/hrmib/* \
	&& poldek --clean-whole \
	&& exit 0
